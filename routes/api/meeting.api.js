const express = require('express');
const passport = require('passport');

const {
  getMeetings,
  createMeeting,
  addUserToMeeting,
  deleteMeeting
} = require('../../controller/meeting.controller');

const { validateCreateMeeting } = require('../../validation/validateMeeting');

const router = express.Router();

router.get('/getAllMeetings', getMeetings);

router.post(
  '/createMeeting',
  passport.authenticate('jwt', { session: false }),
  validateCreateMeeting,
  createMeeting
);

router.post(
  '/addUser/:meetingId',
  passport.authenticate('jwt', { session: false }),
  addUserToMeeting
);

router.delete(
  '/deleteMeeting/:meetingId',
  passport.authenticate('jwt', { session: false }),
  deleteMeeting
);

module.exports = router;
