const express = require('express');
const upload = require('../../config/upload');
const passport = require('passport');

const {
  validateRegister,
  validateLogin
} = require('../../validation/validateUser');
const { User } = require('../../models/User');

const {
  createUser,
  login,
  getUserList,
  getUserHistoryMeeting,
  uploadAvatar
} = require('../../controller/user.controller');

const router = express.Router();

router.post('/register', validateRegister, createUser);
router.post('/login', validateLogin, login);
router.get('/getUsersList', getUserList);
router.post(
  '/uploadAvatar',
  passport.authenticate('jwt', { session: false }),
  upload.single('avatar'),
  uploadAvatar
);
router.get(
  '/getUserMeeting',
  passport.authenticate('jwt', { session: false }),
  getUserHistoryMeeting
);

module.exports = router;
