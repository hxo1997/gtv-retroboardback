const validator = require('validator');

module.exports.validateCreateMeeting = (req, res, next) => {
  let errors = {};
  const { title } = req.body;
  if (validator.isEmpty(title)) {
    errors.title = 'Title of Meeting is required';
  } else if (!validator.isLength(title, { min: 1, max: 50 })) {
    errors.title = 'Title must be 1 - 50 characters';
  }

  // send errors if not valite
  if (Object.keys(errors).length) {
    return res.status(400).json(errors);
  }
  next();
};
