const validator = require('validator');

module.exports.validateRegister = (req, res, next) => {
  let errors = {};
  const { email, password, password2, name } = req.body;

  // check email
  if (validator.isEmpty(email)) {
    errors.email = 'Email required';
  } else if (!validator.isEmail(email)) {
    errors.email = 'Wrong email';
  }

  // check password
  if (validator.isEmpty(password)) {
    errors.password = 'Pasword required';
  } else if (!validator.isLength(password, { min: 6, max: 30 })) {
    errors.password = 'Password must be 6 - 30 characters';
  }

  if (validator.isEmpty(password2)) {
    errors.password2 = 'Confirm pasword required';
  } else if (!validator.equals(password, password2)) {
    errors.password2 = 'Confirm password not match';
  }
  // check name
  if (validator.isEmpty(name)) {
    errors.name = 'name required';
  } else if (!validator.isLength(name, { min: 2, max: 30 })) {
    errors.name = 'Name must be 2 - 30 characters';
  }
  // send errors if not valite
  if (Object.keys(errors).length) {
    return res.status(400).json(errors);
  }
  next();
};

module.exports.validateLogin = (req, res, next) => {
  let errors = {};
  const { email, password } = req.body;
  // check email
  if (validator.isEmpty(email)) {
    errors.email = 'Email required';
  } else if (!validator.isEmail(email)) {
    errors.email = 'Wrong email';
  }

  // check password
  if (validator.isEmpty(password)) {
    errors.password = 'Pasword required';
  } else if (!validator.isLength(password, { min: 6, max: 30 })) {
    errors.password = 'Password must be 6 - 30 characters';
  }

  // send errors if not valite
  if (Object.keys(errors).length) {
    return res.status(400).json(errors);
  }
  next();
};
