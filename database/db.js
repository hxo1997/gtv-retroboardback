const mongoose = require('mongoose');
require('dotenv').config(); // use for .env file
module.exports = () => {
  mongoose.set('useCreateIndex', true);
  mongoose
    .connect(process.env.MONGOOSE_URL, { useNewUrlParser: true })
    .then(console.log('connected to mongodb'))
    .catch(console.log);
};
