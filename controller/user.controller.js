const bcrypt = require('bcryptjs');
const jwt = require('jsonwebtoken');

const { User } = require('../models/User');
const secretKey = process.env.SECRET_KEY;

module.exports.createUser = (req, res) => {
  const { email, password, name } = req.body;
  let errors = {};
  // find if user is existed
  User.findOne({ $or: [{ email }, { name }] }).then(user => {
    if (user) {
      if (user.email === email) errors.email = 'Email already existed';
      if (user.name === name) errors.name = 'Name already existed';
      return res.status(400).json(errors);
    }

    const newUser = new User({
      email,
      password,
      name
    });

    // salt password to secret code and save
    bcrypt.genSalt(10, (err, salt) => {
      if (err) return res.status(400).json(err);
      bcrypt.hash(newUser.password, salt, (err, hash) => {
        if (err) return res.status(400).json(err);
        newUser.password = hash;

        newUser
          .save()
          .then(user => {
            return res.status(200).json(user);
          })
          .catch(console.log);
      });
    });
  });
};

module.exports.login = (req, res) => {
  const { email, password } = req.body;

  User.findOne({ email }).then(user => {
    if (!user) return res.status(400).json({ email: 'Error, email not match' });

    bcrypt
      .compare(password, user.password) // so sánh password nhập vs password dc hash
      .then(isMatch => {
        if (!isMatch)
          return res.status(400).json({ password: 'Wrong password' });

        const payload = {
          id: user._id,
          email: user.email,
          nameame: user.name,
          avatar: user.avatar
        };

        // Nếu trùng khớp thì trả về 1 chuỗi token để xác nhận user đăng nhập
        jwt.sign(payload, secretKey, { expiresIn: '1h' }, (err, token) => {
          res.status(200).json({
            success: true,
            token: 'Bearer ' + token
          });
        });
      });
  });
};
module.exports.getUserList = (req, res) => {
  User.find()
    .then(users => {
      return res.status(200).json(users);
    })
    .catch(err => {
      return res.status(400).json(err);
    });
};
module.exports.uploadAvatar = (req, res) => {
  User.findById(req.user.id)
    .then(user => {
      if (!user) return res.status(400).json({ error: "User doesn't exist" });

      const regexPath = /(png|svg|jpg|jpeg)$/i;
      if (!regexPath.test(req.file.path)) {
        return res.status.json({ error: 'Avatar must be image' });
      }
      user.avatar = req.file.path;
      return user.save();
    })
    .then(user => res.status(200).json(user))
    .catch(err => {
      return res.status(400).json(err);
    });
};
module.exports.getUserHistoryMeeting = (req, res) => {
  const userId = req.user.id;
  Meeting.find()
    .then(meetings => {
      let userMeetingHistory = [];
      meetings.map(meeting => {
        meeting.joiners.map(joiner => {
          if (joiner.joinerId === userId) userMeetingHistory.push(meeting);
        });
      });
      return res.status(200).json(userMeetingHistory);
    })
    .catch(err => res.status(400).json(err));
};
