const { User } = require('../models/User');
const { Meeting } = require('../models/Meeting');

module.exports.getMeetings = (req, res) => {
  Meeting.find()
    .then(meetings => res.status(200).json(meetings))
    .catch(err => res.status(400).json(err));
};

module.exports.createMeeting = (req, res) => {
  const { title } = req.body;

  let newMeeting = new Meetingg({
    createdBy: req.user.id
  });

  newMeeting
    .save()
    .then(meeting => res.status(200).json(meeting))
    .catch(err => {
      return res.status(400).json(err);
    });
};

module.exports.addUserToMeeting = (req, res) => {
  const meetingId = req.params.meetingId;
  const joiners = JSON.parse(req.body);
  Meeting.findById(meetingId)
    .then(meeting => {
      if (!meeting)
        return res.status(400).json({ errors: 'Meeting not found' });
      const newJoiners = joiners;
      meeting.joiners.push(...newJoiners);
      return meeting.save();
    })
    .catch(err => res.status(400).json(err));
};

module.exports.deleteMeeting = (req, res) => {
  Meeting.findByIdAndDelete(req.params.meetingId)
    .then(response => res.status(200).json({ message: 'Deleted Meeting' }))
    .catch(err => err.status(400).json(err));
};
