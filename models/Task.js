const mongoose = require('mongoose');
const Schema = mongoose.Schema;
const TaskSchema = new Schema({
  taskTitle: { type: String, default: 'No Title' },
  content: { type: String, default: 'No Content' },
  date: {
    type: Date,
    default: Date.now
  },
  contributors: {
    type: Schema.Types.ObjectId,
    required: true
  },
  status: {
    type: Number,
    required: true
  },
  createdBy: {
    type: Schema.Types.ObjectId,
    ref: 'User',
    required: true
  },
  dueDate: {
    type: Date,
    default: Date.now
  },
  color: {
    type: String,
    default: '#2196f3'
  },
  meetingId: {
    type: Schema.Types.ObjectId,
    required: true
  }
});
const Task = mongoose.model('Task', TaskSchema);

module.exports = {
  Task,
  TaskSchema
};
