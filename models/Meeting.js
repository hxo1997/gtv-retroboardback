const mongoose = require('mongoose');
const Schema = mongoose.Schema;
const MeetingSchema = new Schema({
  title: {
    type: String,
    maxlength: 50
  },
  createdBy: {
    type: Schema.Types.ObjectId,
    ref: 'User',
    required: true
  },
  createdDate: {
    type: Date,
    default: Date.now
  },
  joiners: [
    {
      joinerId: { type: Schema.Types.ObjectId }
    }
  ]
});
const Meeting = mongoose.model('Meeting', MeetingSchema);

module.exports = {
  Meeting,
  MeetingSchema
};
